# Eclipse Foundation Legal FAQs

This repository contains the AsciiDoc source and build resources required to render legal FAQs and some other related documentation.

Specifically, the sources for the following documents are here:

* [Eclipse Foundation Legal FAQ](./documents/src/adoc/legalfaq.adoc), which is rendered [here](https://www.eclipse.org/legal/legalfaq.php)
* [Eclipse Public License 2.0 FAQ](/documents/src/adoc/epl-2.0-faq.adoc), which is rendered [here](https://www.eclipse.org/legal/epl-2.0/faq.php); and
* [Eclipse Committer Due Diligence Guidelines](documents/src/adoc/committers-dd.adoc), which is rendered [here](https://www.eclipse.org/legal/committerguidelines.php).

## Style

We use AsciiDoc to capture the contents and render using [Asciidoctor](http://www.asciidoctor.org).

The following authoring styles apply:

* A single paragraph occupies as single line (i.e. soft wrapping);
* All defined terms (e.g. _Specification Project_) use pseudo-legal _title case_; and
* Where possible images are rendered as `SVG`.

## Build

Commits are **not** automatically published to the live website.

The Maven `pom.xml` file will generate HTML version of the documents. Note that the HTML is generated as `body` content only (i.e. no header) as it is intended to be included as part of a web page on the Eclipse.org website and leverage the styles defined in that site's CSS. All images are rendered inline.

````bash
mvn clean generate-resources
````

# License

This content is licensed under the EPL-2.0.
