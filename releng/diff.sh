#!/bin/bash
# Copyright (C) Eclipse Foundation, Inc. and others. 
# 
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License v. 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
# 
# SPDX-License-Identifier: EPL-2.0

# This is a quick and dirty utility for generating a diff between the v1.2
# version of the EFSP and the version that is under development and moving
# that diff into the www.eclipse.org/projects directory (where it can be
# committed to update the website). That is, for this script to be of any
# value, the www.eclipse.org/projects Git repository must be cloned into
# /gitroot/www.eclipse.org/projects on the local file system.
# 
# Further, this script should be run after the Maven build script is run
# to produce an update to the new version of the EFSP.
#
# TODO incorporate this script into the Maven build.
# 
# This script likely only runs on Wayne's desktop.

cd "${BASH_SOURCE%/*}"
npm i node-htmldiff
git show epl-2.0-faq-update:./documents/src/adoc/epl-2.0-faq.adoc | asciidoctor -a nofooter -a stylesheet=/gitroot/www.eclipse.org/projects/handbook/resources/handbook.css -d book -o /tmp/epl-2.0-faq-update.html -
git show epl-2.0-faq-baseline:../documents/src/adoc/epl-2.0-faq.adoc | asciidoctor -a nofooter -a stylesheet=/gitroot/www.eclipse.org/projects/handbook/resources/handbook.css -d book -o /tmp/epl-2.0-faq-original.html -
node ./node_modules/node-htmldiff/htmldiff-cli.js /tmp/epl-2.0-faq-original.html /tmp/epl-2.0-faq-update.html  /tmp/epl-2.0-faq-diff.html

git show legalfaq-update:../documents/src/adoc/legalfaq.adoc | asciidoctor -a nofooter -a stylesheet=/gitroot/www.eclipse.org/projects/handbook/resources/handbook.css -d book -o /tmp/legalfaq-update.html -
git show legalfaq-baseline:../documents/src/adoc/legalfaq.adoc | asciidoctor -a nofooter -a stylesheet=/gitroot/www.eclipse.org/projects/handbook/resources/handbook.css -d book -o /tmp/legalfaq-original.html -
node ./node_modules/node-htmldiff/htmldiff-cli.js /tmp/legalfaq-original.html /tmp/legalfaq-update.html  /tmp/legalfaq-diff.html
