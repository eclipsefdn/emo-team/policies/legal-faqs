[#legalfaq]
= Eclipse Foundation Legal Frequently Asked Questions (FAQ)

Last updated on {localDate}

[#overview]
== Overview

[[h.mkk9i18tsofa]]

The following FAQ answers questions regarding legal issues related to Eclipse Projects, covering such topics as:

* licensing of materials made available by the Eclipse Foundation;
* contributions submitted to the Eclipse Foundation;
* working as a committer on Eclipse projects; and
* cryptography in software made available by the Eclipse Foundation

If you intend to use, modify, and/or redistribute materials made available by the Eclipse Foundation, you may find this FAQ useful.

This FAQ is provided for informational purposes only. It is not part of, nor does it modify, amend, or supplement the terms of licenses, or any other legal agreement. If there is any conflict between this FAQ and a legal agreement discussed herein, the terms of the discussed legal agreement shall govern. This FAQ should not be regarded as legal advice. If you need legal advice, you must contact your own lawyer.

If you have a question that you would like to see answered in this FAQ, please send e-mail to the Eclipse Licensing alias (license@eclipse.org).

[#use]
== Use and Redistribution of Content

[[h.nh8z9duojqz0]]

[qanda]

[[h.skv48lga3op6]] Does the Eclipse Foundation own the content that it distributes? ::

No. All of the materials distributed by the Eclipse Foundation are owned by their respective copyright holders. Eclipse Foundation projects use what is sometimes referred to as “symmetrical inbound/outbound licensing”, which means that contributions are accepted under the same license that they are distributed under. As a result, the Eclipse Foundation itself does not have any particular ownership or licensing stake in the materials it distributes.

[[h.1legecavy5xx]] May I please have a quote or see a price list for Eclipse software? ::

Eclipse software is free and open source software that is distributed royalty-free under the terms of the project license(s). If you are going to distribute Eclipse code, you should read our https://www.eclipse.org/projects/handbook#legaldoc[Guidelines] and our https://www.eclipse.org/org/documents/epl-2.0/faq.php[EPL FAQ]. They both contain a great deal of useful information.

[#licenses]
== Licenses

[qanda]

[[use-notices]][[h.qfp99yh9nfdc]] How do projects convey licensing and related information? ::

Eclipse projects convey licensing information via https://www.eclipse.org/projects/handbook/#legaldoc-license[license] and https://www.eclipse.org/projects/handbook/#legaldoc-notice[notice] files. A license file, typically named LICENSE, contain the exact text of the project license(s) and are located in the root of source code repositories and in the distribution content. Notices files, typically named NOTICE, contain information about the project license(s), how they are combined, copyright holders, cryptography, and other project metadata.

[[h.fzvxw8u5p8jr]] What is the Eclipse Public License (EPL)? ::

The https://www.eclipse.org/legal/epl-2.0#[Eclipse Public License (EPL)] is an open source license that has been approved by the https://www.opensource.org/licenses/#[Open Source Initiative].

[[h.wxfu8ynj36kj]] Why is the Eclipse Public License (EPL) used to license most of the content made available by the Eclipse Foundation? ::

The terms and conditions of the EPL were crafted to be most appropriate for the Eclipse Foundation. Software made available by the Eclipse Foundation is used by developers who want to freely plug into and extend or alter the content. The EPL goes to great lengths to support and encourage the collaborative open source development of the content, while maximizing the ability to use and/or integrate the content with software licensed under other licenses, including many commercial licenses. Since the Eclipse Foundation seeks to encourage developers of both open source and proprietary development tools to embrace the content, this flexibility is critical.

[[h.8hw4xp7chejr]] What other licenses (besides the Eclipse Public License) may be used by Eclipse Open Source Projects? ::

Eclipse Projects have a Declared License that applies to content developed and maintained by the project team (“Project Code”). Most Eclipse projects use the Eclipse Public License (EPL) as their Declared License, but other licenses (e.g. the Apache Software License 2.0) can be used. All Eclipse Projects exist under the umbrella of a https://www.eclipse.org/projects/dev_process/#4_Structure_and_Organization[Top-Level Project], each of which specifies one or more permissible licensing schemes. Any licensing scheme other than what is specified by a Top-Level Project must be approved by the Eclipse Board of Directors.
+
Eclipse Projects may also distribute third party content under different licenses (which are compatible with the project license). The https://www.eclipse.org/legal/licenses.php#[Third Party Content Licenses] page provides a list of the most common licenses approved for use by third party content redistributed by Eclipse Foundation Projects. Queries regarding a specific license, should be directed to Eclipse Licensing (license@eclipse.org).
+
https://www.eclipse.org/projects/handbook/#legaldoc-license[License] and https://www.eclipse.org/projects/handbook/#legaldoc-notice[notice] files that accompany the content produced by an Eclipse Project describe the licenses that apply.

[[h.9dhzr9lvkqh8]] Why does the Eclipse Foundation redistribute some content under licenses other than the Eclipse Public License? ::

Sometimes the Project Management Committee (PMC) for an Eclipse Foundation Top-Level project may make a decision to include some content from another (third party) open source project. If that content cannot be contributed to an Eclipse Foundation project under the https://www.eclipse.org/legal/termsofuse.php#[Eclipse.org Terms of Use] and the content is not already licensed under EPL, the PMC may decide to use and redistribute the content under the terms and conditions of another license which would usually be the license that the content was received under.
+
The Eclipse Foundation will only use non-EPL licensed content if the other license is an open source license approved by the https://www.opensource.org/licenses/#[Open Source Initiative] and it permits commercial products to be built on the software without requiring any form of royalty or other payment.
+
In some cases the Eclipse Foundation redistributes unmodified non-EPL content and in other cases it redistributes derivative works of non-EPL content. Unmodified non-EPL content is included as a convenience to allow Eclipse Foundation software to be used without first having to locate, obtain, and integrate additional software.

[[h.rnnp3o5bxq5]] What licenses are acceptable for third-party content redistributed by Eclipse Projects? ::

The Eclipse Foundation views license compatibility through the lens of enabling successful adoption of Eclipse technology in both open source and in commercial software products and services. We wish to create a ecosystem based on the redistribution of Eclipse software technologies which includes commercially licensed software products. Determining whether a license for third-party content is acceptable often requires the input and advice of Eclipse’s legal advisors.
+
The https://www.eclipse.org/legal/licenses.php#[Third Party Content Licenses] page provides a list of the most common licenses approved for use by third party content redistributed by Eclipse Foundation Projects. If you have any questions, please contact Eclipse Licensing (license@eclipse.org).

[[h.r2y1yck33rg4]]
=== I see copyright notices from IBM and/or other companies. How can it be open source software if it is copyright IBM?

You might want to do some background reading on copyright law or consult a lawyer. "Open source" doesn’t mean that the code does not have a copyright holder. All code has an author and that person is the copyright holder or owner unless the copyright is assigned to another party. In the case of the Eclipse Project, the initial code base was contributed by IBM. Over time, Eclipse Foundation projects have become populated with code provided by many different contributors and as result, different portions of the code have different copyright holders. Licenses such as the EPL grant you a copyright license (subject to certain terms and conditions) and that is how you receive copyright rights to use, modify, redistribute, etc. the content. Although you may receive copyright rights through the EPL, or another license, it still doesn’t change who the copyright holders are for various portions of the content.

[[version]] Which version of the Eclipse Public License does the Eclipse Foundation use? ::

The current version is https://www.eclipse.org/legal/epl-2.0#[Version 2.0]. There is a separate https://www.eclipse.org/org/documents/epl-2.0/faq.php#[FAQ] that focuses entirely on the EPL.

[[h.25gqlkbku1u9]] Does the Eclipse Foundation have a patent policy? ::

Under the Eclipse Public License (EPL), each Contributor grants rights to create derivative works and for worldwide, royalty-free software redistribution in accordance with the EPL terms, including a royalty-free license to use Contributor’s patents as embodied in its contributions. Section 7 of the EPL includes the following:
+
____
If Recipient institutes patent litigation against any entity (including a cross-claim or counterclaim in a lawsuit) alleging that the Program itself (excluding combinations of the Program with other software or hardware) infringes such Recipient’s patent(s), then such Recipient’s rights granted under Section 2(b) shall terminate as of the date such litigation is filed.
____

== Contributions and Participation

[[h.2gex0eipl1id]]

[qanda]

[[h.reim5nd6ep3m]] What agreement covers contributions I submit to an Eclipse Foundation project? ::

Contributors who are invited to join an Eclipse open source project must complete additional https://www.eclipse.org/projects/handbook/#paperwork[committer paperwork] before they are granted write access to project resources. The Eclipse Foundation needs to ensure that all committers with write access to the code, websites, and issue tracking systems understand their role in the intellectual property process. The Eclipse Foundation also needs to ensure that we have accurate records of the people who are acting as change agents on the projects. To ensure that committers understand their role, and that the Eclipse Foundation has accurate records, committers must provide documentation asserting that they have read, understood, and will follow the committer guidelines. Committers must also gain their employers consent to their participation in Eclipse Foundation open source projects.

[[h.26hpwm6wrcbu]] Will Eclipse projects accept contributions to content that were not provided under the project license? ::

The short answer to this question is "it depends".
+
Content made available by the Eclipse Foundation is sometimes accompanied by or includes content based on third-party content. This third-party content is often sourced from other open source projects such as those run by the Apache Software Foundation. Eclipse Foundation projects try to avoid branching from other open source projects wherever possible. However sometimes it just can’t be avoided especially if the change is critical and the other project’s next release date is too far out or they won’t accept the change for some reason. Subsequently, while changes to third-party content (either unmodified or derivative works of) might be accepted and incorporated into the Eclipse Foundation codebase, Eclipse Foundation committers may often forward these changes to other open source projects and/or may ask contributors of such changes to do the same.

[[h.l3bvwrmty0iz]] What if I have legal questions about my contribution to the Eclipse Foundation? ::

If the questions you have concern whether or not you can legally contribute the content to the Eclipse Foundation, you should consult a lawyer. If you are contributing content on behalf of the company you work for then you should probably ask your company’s legal department. It is up to you to ensure that you can satisfy section 2 d) of the EPL that says:
+
____
Each Contributor represents that to its knowledge it has sufficient copyright rights in its Contribution, if any, to grant the copyright license set forth in this Agreement.
____
+
Many companies have their own processes for handling contributions to open source projects.

[#committer]
== Working as a Committer

[[h.dris33ugmjs]]

[qanda]

[[h.egfvt1pcqyla]] What are my obligations as a committer working on an Eclipse Foundation project? ::

As a committer you have the ability to control the content distributed in the Eclipse Foundation source code repositories and downloads. Contributors may provide contributions to the Eclipse Foundation but such contributions do not exist in the repository or downloads unless they are accepted by a committer. Committers therefore have a responsibility to perform due diligence on any content they release.

[[h.dw2jc7fvtv5t]] What is the due diligence that a committer must perform? ::

The https://www.eclipse.org/legal/committerguidelines.php#[Eclipse Committer Due Diligence Guidelines] outline the guidelines for committers.

[[h.ifqjlo6ueqc7]] What if I have legal questions when I am performing due diligence as a committer? ::

You should contact the Project Management Committee (PMC) for your project.

[[h.x0s5sxj1z2cp]] Do I have to place copyright notices in all my code? ::

Yes. Where possible, copyright notices must be included the https://www.eclipse.org/projects/handbook/#ip-copyright-headers[file headers] for all project content. This includes source files and, where possible, configuration. A best effort approach is expected, and so reasonable exceptions are acceptable (e.g. there is not standard means for providing a file header in JSON content). See the https://www.eclipse.org/projects/handbook/#ip-copyright-headers[default copyright and license notice template].

[[h.tq9t96dkfm2y]] If I change employers, what happens to my committer status at Eclipse? ::

Your committer status at the Eclipse Foundation is not based on your employment status. It is an individual recognition of your frequent and valuable contributions to one or more Eclipse projects. If you do change employers, please contact the EMO Records Team by email (emo-records@eclipse.org)  to ensure that the necessary employer consent paperwork is completed. For more details, please see the https://www.eclipse.org/projects/handbook/#elections-committer[New Committer Process].

[#cryptography]
== Cryptography

[[h.qu58e71q3dbc]]

[qanda]

[[h.xqxr6abpyq6y]] Does software made available by the Eclipse Foundation contain cryptography? ::

In some cases software made available by the Eclipse Foundation may contain cryptography. 
+
https://www.eclipse.org/projects/handbook/#legaldoc-notice[Notice files] (e.g. an about.html files in Eclipse Platform Plug-ins)  contain information about the specific algorithms, key sizes, and other important information that may be required to obtain additional export control classifications and approvals.

[[h.skw921k3c85n]] Why is cryptography discussed in notices? ::

Some countries have restrictions regarding the export, import, possession, and use, and/or re-export to another country, another person or for a particular use of encryption software, but it does not restrict you in any way nor require you to do anything special if you receive encryption software from the Eclipse Foundation. In other words, it is your responsibility to determine what laws and regulations apply to you and to act appropriately.